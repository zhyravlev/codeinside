'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();

gulp.task('sass', function () {
    return gulp.src('./src/css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./src/css'));
});

gulp.task('default', ['sass'], function() {
    browserSync.init({
        port: 9000,
        server: {
            baseDir: "./",
            index: "./src/index.html",
            routes: {
                "/css": "./src/css",
                "/js": "./src/js",
                "/templates": "./src/templates",
                "/bower_components": "../bower_components"
            }
        }
    });
});