var ratingApp = angular.module('ratingApp', [
    'ngRoute',
    'ratingCtrl'
]);

ratingApp.config(['$routeProvider', function($routeProvider) {
$routeProvider.
    when('/', {
        templateUrl: 'templates/main.html',
        controller: 'mainCtrl',
        title: 'Главная страница'
    }).
    when('/rating', {
        templateUrl: 'templates/rating.html',
        controller: 'ratingListCtrl',
        title: 'Оценки'
    }).
    when('/classes', {
        templateUrl: 'templates/classes.html',
        controller: 'classesCtrl',
        title: 'Пропуски'
    }).
    otherwise({
        redirectTo: '/'
    });
}])

.run(['$location', '$rootScope', function($location, $rootScope) {
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {

        if (current.hasOwnProperty('$$route')) {
            $rootScope.title = current.$$route.title;
        }

        var nav = document.getElementsByClassName('navigation-main')[0],
            link = nav.getElementsByTagName('a');

        for(var i = 0; i <= link.length-1; i++){

            var a = angular.element( link[i] );

            if(link[i].hash.replace('#','') == $location.url())
                a.addClass('active');
            else
                a.removeClass('active');
        }
    });
}]);