(function(){
    'use strict';

    var data = {
            student: 'Антон Журавлёв',
            bally_total: 0,
            rating:[{value:5},{value:4},{value:5}],
            bally_average: 0,
            employment_total: 20,
            respectful_reason: 2,
            respectful_not_reason: 1,
            not_reason_total: 0
        };

    var not_reason_total = function(){

        var percent = ((data.respectful_not_reason / data.employment_total) * 100).toFixed(2);
        data.not_reason_total = percent;
        return percent;
    };

    var bally_average = function(){

        var result, count = 0;

        angular.forEach(data.rating, function(d) {
            count += d.value;
        });

        result = (count / data.rating.length).toFixed(2);
        return isNaN(result) ? ' --- ' : result;
    };

    /* -------------------------------------------------------------------------------------- */

    var ratingCtrl = angular.module('ratingCtrl', []);

    ratingCtrl.controller('mainCtrl', ['$scope', '$http', function($scope, $http) {
        for(var key in data){
            if(data.hasOwnProperty(key)){
                switch(key){
                    case 'bally_total': $scope[key] = data.rating.length; break;
                    case 'bally_average': $scope[key] = bally_average(); break;
                    case 'not_reason_total': $scope[key] = not_reason_total(); break;
                    default: $scope[key] = data[key];
                }
            }
        }

        $scope.yea = (data.not_reason_total < 10) ? true : false;

        $scope.send = function(){

            var data = angular.toJson(data, false);

            $http({
                method: 'GET',
                url: './src/server.html',
                data: { data: data }
            }).then(function(response) {
                if(response.status == 200){
                    alert('Данные отправлены на сервер.');
                }
            }, function(response) {
                if(response.status == 404){
                    alert('Ошибка отправки данных: ' + response.statusText);
                }
            });
        }
    }]);

    ratingCtrl.controller('ratingListCtrl', ['$scope', function($scope) {

        $scope.data = data.rating;
        $scope.bally_average = bally_average();

        $scope.add = function(ratingForm){
            if(ratingForm.$valid){

                data.rating.push({
                    value: Number($scope.value)
                });

                $scope.value = '';
                $scope.data = data.rating;
                $scope.bally_average = bally_average();
            }
        };

        $scope.remove = function(event){

            var em, index;

            em = angular.element( event.target );
            index = em.attr('data-index');

            data.rating.splice(index, 1);

            $scope.bally_average = bally_average();
        };
    }]);

    ratingCtrl.controller('classesCtrl', ['$scope', function($scope) {

        var variables = ['employment_total','respectful_reason','respectful_not_reason'];

        variables.forEach(function(key){
            $scope[key] = data[key];
        });

        $scope.save = function(classesForm){
            if(classesForm.$valid){
                variables.forEach(function(key){
                    data[key] = $scope[key];
                });

                var em = angular.element( document.getElementById('saved') );
                    em.addClass('in');

                setTimeout(function(){
                    em.removeClass('in');
                }, 1000);

                not_reason_total();
            }
        }
    }])
})();
